// Use the "require" directive to load the express module/package
const express = require("express")

const mongoose = require("mongoose")


//Create an application express
const app = express()


// For our application server to run, we need a port to listen to
const port = 3000

// MongoDB connection

/*

mongoose.connect("<MongoDB connection string>,{useNewURLParser : true")


*/

//Connecting MongoDB Atlas
//Add password and database name
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.x516lxe.mongodb.net/s35?retryWrites=true&w=majority",{
	//due to update in MongoDB drivers that allows connection to it, the defualt connection is being flagged as error
	// allows us to avoid any current and future erro while connecting to MongoDB
	useNewUrlParser : true,
	useUnifiedTopology: true
})



// Connecting to MongoDB locally

let db = mongoose.connection

db.on("error", console.error.bind(console,"connection error"))
db.once("open",()=>console.log("Connected to MongoDB Atlas"))





//Allows app to read json data
app.use(express.json())

// Allow your app to read other data types
app.use(express.urlencoded({extended: true}))




// Mongoose Schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// Models
// Models must be in singular form and capitalized
const Task = mongoose.model("Task", taskSchema)

// Creating a new Task
/*
Business Logic
 Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post("/tasks", (req, res)=>{

	// To check if there are duplicate tasks
	Task.findOne({name: req.body.name}, (err, result) =>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found!")
		}else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr,savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New task created")
				}
			})

			
		}
	})


})


// Get all the tasks

/*
Business Logic
Retrieve all the documents
If an error is encountered, print the error
If no errors are found, send a success status back to the client/Postman and return an array of documents
*/


app.get("/tasks",(req,res)=>{
	Task.find({},(err,result) => {
		if(err){
			return console.log(err)
		}else {
			return res.status(200).json({
				data: result
			})
		}
	})
})


//Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`))


const express = require("express")

const mongoose = require("mongoose")


const app = express()


const port = 5000


mongoose.connect("mongodb+srv://admin123:admin123@cluster0.x516lxe.mongodb.net/s35-activity?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true
})



let db = mongoose.connection

db.on("error", console.error.bind(console,"connection error"))
db.once("open",()=>console.log("Connected to MongoDB Atlas"))






app.use(express.json())

app.use(express.urlencoded({extended: true}))






const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
})

const User = mongoose.model("User",userSchema)


app.post("/signup",(req,res) => {
	User.findOne({username: req.body.username}, (err,result)=>{
		if(req.body.username !== '' && req.body.username !== undefined && req.body.password !== '' && req.body.password !== undefined)
		{
			if(result != null && result.username === req.body.username){
				return res.send("User already exist!")
			}else {
				let newUser = new User({
					username: req.body.username,
					password: req.body.password

				})

				newUser.save((saveErr,savedUser)=>{
					if(saveErr){
						return console.error(saveErr)
					}else{
						return res.status(201).send(`User ${req.body.username} registered!`)
					}
				})

			}
		}else {
			return res.send(`Must fill up both username and password!`)
		}

	})
})






app.listen(port, () => console.log(`Server running at port ${port}`))








